### What's Inside

* [**`datasheet/`**](datasheet/) - contains the datasheet of the following components: *MAX14757* (Quad SPST +70V Analog Switches), *MAX7311* (2-Wire-Interfaced 16-Bit I/O Port Expander with Interrupt and Hot-Insertion Protection), and *MAX7367* (4-Channel I2C Switches/Multiplexer)
* [**`daughtercard/`**](daugtercard/) - contains schematics of the daughtercards to be tested
* [**`manual/`**](manual/) - contains the manual of FTDI devices, HP34401A multimeter, and the USB modules
* [**`others/`**](others/) - contains other files such as MAX7311 evaluation kit and necessary driver software for the USB module