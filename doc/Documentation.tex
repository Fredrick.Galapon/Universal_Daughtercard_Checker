%++++++++++++++++++++++++++++++++++++++++
% Don't modify this section unless you know what you're doing!
\documentclass[letterpaper,11pt]{article}
\usepackage{tabularx} % extra features for tabular environment
\usepackage{amsmath}  % improve math presentation
\usepackage{graphicx} % takes care of graphic including machinery
\usepackage[letterpaper, total={6.5in, 9in}]{geometry} % decreases margins
\usepackage{cite} % takes care of citations
\usepackage[final]{hyperref} % adds hyper links inside the generated pdf file
\hypersetup{
	colorlinks=true,       % false: boxed links; true: colored links
	linkcolor=blue,        % color of internal links
	citecolor=black,       % color of links to bibliography
	filecolor=black,		   % color of file links
	urlcolor=blue         
}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
 
\setlength{\parindent}{2em}
\setlength{\parskip}{0.6em}
\renewcommand{\baselinestretch}{1.2}
\renewcommand{\thesection}{\arabic{section}}%
\usepackage{indentfirst}

\newcommand{\person}[2] {
	\large #1 \\ \small \textit{#2} \\
}

\usepackage{graphicx}
\graphicspath{ {img/} }
%++++++++++++++++++++++++++++++++++++++++

\begin{document}

\begin{titlepage}
	\begin{center}
    	{\scshape \Large Maxim Philippine Operating Corporation\\Test Systems Deparment\par}
		\vfill
        {\huge\bfseries Universal Daughtercard Checker\par}
        {\scshape\Large Project Documentation\par}
        \vfill
        \person{Evidente, Gio Ishmael}{Intern}
        \vspace{0.2in}
        \person{Galapon, Fredrick Angelo}{Intern}
        \vfill
        Supervised by:\par
        \person{Mr. Bruce Allen}{Senior Manager}
		\vfill
		June 2017 - July 2017
	\end{center}
\end{titlepage}

\vspace*{\fill}
    \begin{abstract}
This universal checker project is designed to test different HSXX/BBSXX daughtercards. Basically, it is composed of a large multiplexer that can route meters and input signals to the pins of a daughtercard under test, and control modules of the daughtercard. In this way, faulty components on the card can be diagnosed.
    \end{abstract}
\vspace*{\fill}

\thispagestyle{empty}
\clearpage
\pagenumbering{arabic} 

\section{Project Background}

In a testing procedure, tester-handler equipment is used to test different integrated circuit (IC) chips. This requires a motherboard and different daughtercards to provide the interface between the equipment and ICs. The problem is: when a testing procedure fails, the operator must debug which part of the procedure is faulty, whether the test program, motherboard, or daughtercard. Moreover, when these daughtercards are produced, it must be checked first whether they are working or not before deploying them into test operations. And so, the purpose of this project is to ease the process of debugging by creating a hardware checker for the daughtercards, and effectively determine faulty cards.


\subsection{Daughtercards}
These daughtercards, each having a number of pins ranging from 100 to 240, consist of relays, amplifiers, field-programmable gate arrays (FPGAs), synchronous dynamic random access memories (SDRAMs), etc. These cards provide analog/digital signals and routing, among others, to the motherboard where ICs under test are placed. Some of the daughtercard schematics are provided along with this document.

\subsection{4-Terminal Sensing}
In measuring components such as relays and ICs, 4-Terminal Sensing  or Force-Sense method is used to acquire more accurate measurements than the usual 2-Terminal Sensing. It uses two pairs of probes, called current-carrying and voltage-sensing probes, to accurately measure the impedance of a device. This method is practical for testing components on hardware boards and cards.


\section{Design Requirements and Considerations}

The meter, scope, and input signals must be used to control the daughtercard and determine defective components on it. Because these cards have hundreds of pins and have different components, it would be inconvenient if multiple probes or inputs would be placed one by one. To efficiently measure and probe these components, there must be a way to route the meter, scope, and input signals to each pin of the card. A matrix of analog switches, as shown in Figure \ref{fig:switch-matrix}, can be used to route different inputs to each pin --- which is essentially a large multiplexer.

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[scale=0.4]{project_concept2.png}
\par\end{centering}
\caption{Matrix of controllable switches \label{fig:switch-matrix}}
\end{figure}

Here, the switches must be controlled independently to connect an input to a certain pin. To do so, \href{https://datasheets.maximintegrated.com/en/ds/MAX14756-MAX14758.pdf}{MAX14756 Quad SPST Analog Switches} will be used and \href{https://datasheets.maximintegrated.com/en/ds/MAX7311.pdf}{MAX7311 16-Bit I/O Port Expander} will provide switch controls. With these components, the switch matrix or multiplexer can be constructed. One terminal of each single-pole, single-throw (SPST) switch of MAX14756 will be connected to an input, while the other one will be connected to one pin of the daughtercard. For the switch control, each digital output bits of MAX7311 will drive the enable inputs of MAX14756. MAX14756 provides normally closed analog switches which closes at logic 0 and opens at logic 1. This was chosen because at power-on reset, all MAX7311 output bits is at logic high which effectively turns off all analog switches. 

Communicating with MAX7311 is done through I$^2$C (Inter-Integrated Circuit) interface and so, an FTDI USB module, \href{http://www.ftdichip.com/Support/Documents/DataSheets/Modules/DS_UM232H.pdf}{UM232H}, will be used to read and write data to the MAX7311s. Also, UM232H is capable of Serial Peripheral Interface (SPI), JTAG, and Universal Asynchronous Receiver/Transmitter (UART) communication protocols which are useful for controlling other components of a daughtercard such as FPGAs and SDRAMs.

\subsection{Modular design}
Considering the number of pins of a daughtercard, a lot of analog switches and switch controls will be needed. If all of these will be fabricated on a single board, printed circuit board (PCB) construction and assembly will be strenuous, and testing and debugging of the board itself will be difficult. And so, the design must be modular and expandable.

Given that each MAX14756 contains 4 switches connecting 4 inputs to a single daughtercard pin, a single MAX7311 can only control 4 MAX14756s. For a 200-pin daughtercard, it will require 200 MAX14756s and 50 MAX7311s to connect a set of 4 inputs. This will double if another set of 4 inputs for other input signals is added. Referring to its datasheet, MAX7311 only supports 64 slave addresses for I$^2$C communication; also, if a hundred of MAX7311s is connected to a single I$^2$C bus, each having 10 pF input capacitance, it will exceed the maximum bus capacitance for I$^2$C which is 400 pF. To solve this, \href{https://datasheets.maximintegrated.com/en/ds/MAX7367-MAX7369.pdf}{MAX7367 4-Channel I$^2$C Switches} will be used to be able to distribute the MAX7311s to 4 I$^2$C buses and repeat slave addresses, effectively connecting multiple MAX7311s to a single I$^2$C bus.

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{mux_module.png}
\par\end{centering}
\caption{Multiplexer module \label{fig:mux-module}}
\end{figure}

From the limitations discussed, the large multiplexer must be divided into smaller blocks; a single multiplexer module, shown in Figure \ref{fig:mux-module}, is proposed. The module consists of 4 MAX7311s and 16 MAX14756s in which every MAX7311s, labeled \texttt{EN1} to \texttt{EN4}, controls a line of switches connected to 16 daughtercard pins. With this, 10 modules will be enough to route 4 inputs for a daughtercard with 150 pins, and more of these can be used to expand the multiplexer. So, the main board will only have slots/connectors for the USB and multiplexer modules.

\subsection{Module grouping}
With the modular design, it is important to consider how many modules will be connected to an I$^2$C bus. As mentioned above, a MAX7311 has 10 pF input capacitance; hence, there is a total of 40 pF for each module. Given that the maximum bus capacitance is 400 pF, a maximum of 10 modules can be connected to an I$^2$C bus. Furthermore, taking into account the MAX7311 slave addresses selected through their address pins, the I$^2$C bus lines, SCL (Serial Clock Line) and SDA (Serial Data Line), will also drive the pins mentioned which contributes to the bus capacitance as well. For a safe assumption, 5 modules (total of 20 MAX7311s) on a single bus is set as the maximum.

Figure \ref{fig:conn-grp} shows how the setup and connections of 5 modules will be for each I$^2$C bus. There will be 5 module connectors, named \texttt{X1} to \texttt{X5}, with different address pin connections for each MAX7311 on every module. The connections of these pins, \texttt{EN1\_AD} to \texttt{EN4\_AD}, will be done on the main board so that the multiplexer module will be generic and interchangeable. 

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{connector_group.png}
\par\end{centering}
\caption{Module connectors \texttt{X1} to \texttt{X5} on a single I$^2$C bus \label{fig:conn-grp}}
\end{figure}

\subsection{Main board}
Continuing from the devised module design and the group of modules/connectors, the whole multiplexer can be constructed as desired. For the project, the target number of daughtercard pins to be handled is 320 with 8 inputs to be routed. The modules on connectors \texttt{X1} to \texttt{X5} can route 4 inputs to a total of 80 daughtercard pins. So, for a set of 4 inputs and 320 pins, 4 sets of connectors \texttt{X1} to \texttt{X5} are required. Hence, for two sets of 4 inputs, the main board will have a total of 8 sets of connectors (total of 40 modules), along with the control block and a connector for all the daughtercards. The main board is shown in Figure \ref{fig:main-brd}.

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[angle=90, width=6.5in,keepaspectratio]{uni_checker_main_board.png}
\par\end{centering}
\caption{Project main board\label{fig:main-brd}}
\end{figure}

As shown, the input block consists of 4-wire meter probes and other 4 input signals. The control block is composed of 2 MAX7367 I$^2$C channel switches, UM232H, and one MAX7311 to provide the main I$^2$C channel routing and control signals for daughtercard under test. The 4 channels of each MAX7367, \texttt{I2CSW1} and \texttt{I2CSW2}, are dedicated for the switch controls of 4 groups of connectors \texttt{X1} to \texttt{X5}---which route a set of 4 inputs to 320 pins. Also, UM232H has two buses, ADBUS and ACBUS, that can be configured as GPIO, clock, and data lines for different serial/parallel communication protocols; two lines from ADBUS will be configured as the main I$^2$C lines, SCL and SDA.

Furthermore, the main connector on the board is dedicated for different adapter boards suited for different daughtercards. This contains specific connector for each daughtercard and routes the necessary multiplexer outputs and control signals to the daughtercard. 

As a summary, listed below are the main components and connectors on the board.
\begin{itemize}
  \item Input meter probes: 8 banana jacks
  \item Multiplexer module connectors: total of 40 50-pin vertical card edge connectors
  \item UM232H connector: 28-pin DIP socket
  \item 2 MAX7367 I$^2$C switches
  \item 1 MAX7311 port expander
  \item Adapter board connector: 2 150-pin and 1 60-pin horizontal card edge connectors
  \item Power supply: DC jacks or banana jacks for +5V and +/-15V
\end{itemize}

\subsection{Multiplexer control}
To be able to control the analog switches connected to each daughtercard pin, it is necessary to write data to MAX7311 specifically, depending on which input and pin should connect. The MAX7311s, \texttt{EN1} to \texttt{EN4}, on each module on connectors \texttt{X1} to \texttt{X5} will have 20 distinct slave addresses, repeated for every channel; these addresses are summarized in Table \ref{tab:slave-addr} and their AD pin configurations are listed in \href{https://datasheets.maximintegrated.com/en/ds/MAX7311.pdf}{MAX7311 datasheet} (p. 12). From the desired input and pin number, the method for controlling a switch is as follows:

\begin{enumerate}
   \item Communicate with MAX7367: \texttt{I2CSW1} for 4-wire meter, \texttt{I2CSW2} for other input signals (refer to \href{https://datasheets.maximintegrated.com/en/ds/MAX7367-MAX7369.pdf}{MAX7367 datasheet} for commands)
   \item Enable specific channel depending on the pin number and disable other channels:
   \begin{itemize}
   		\item Channel 1: Pins 1-80
   		\item Channel 2: Pins 81-160
   		\item Channel 3: Pins 161-240
   		\item Channel 4: Pins 241-320
	\end{itemize}
   \item Calculate specific \texttt{ENX} address (in hex): 
   \begin{itemize}
   		\item lower nibble $\Rightarrow$ correlates with input number
    	\item upper nibble $\Rightarrow$ depends on which connector \texttt{Xn} \texttt{ENX} is connected
	\end{itemize}
    \item Communicate with the MAX7311 with calculated address
    \item Write a byte with a bit set to 0 on the target pin, all other bits are one
\end{enumerate}

\begin{table}[!htbp]
\caption{Summary of MAX7311 Slave Addresses }
\centering
\begin{tabular}{c | c c c c}
\hline \hline
Connector & EN1 & EN2 & EN3 & EN4	\\ [0.5ex]
\hline
X1 & \texttt{0x20} & \texttt{0x22} & \texttt{0x24} & \texttt{0x26}\\
X2 & \texttt{0x30} & \texttt{0x32} & \texttt{0x34} & \texttt{0x36}\\
X3 & \texttt{0x40} & \texttt{0x42} & \texttt{0x44} & \texttt{0x46}\\
X4 & \texttt{0x50} & \texttt{0x52} & \texttt{0x54} & \texttt{0x56}\\
X5 & \texttt{0xA0} & \texttt{0xA2} & \texttt{0xA4} & \texttt{0xA6}\\
\hline
\hline
\end{tabular}
\label{tab:slave-addr}
\end{table}

\section{Software Development}
Now that the board has already been designed, it is necessary to create a program that could control the daughtercard checker. The main consideration for the program is the communication of I$^2$C devices on the board and the UM232H as the controller. And so, the UM232H is configured for I$^2$C interface, and I$^2$C protocol functions were created. In addition to this, communication with the HP34401A digital multimeter (DMM) was also developed for writing and reading commands in order to acquire measurements.

\subsection{\texorpdfstring{I$^2$C}--Interface using USB245M and UM232H}
To be able to communicate properly with the different devices on the board, such as MAX7367 and MAX7311, I$^2$C protocol would be implemented. It can support multi-master, multi-slave systems, and uses only two bus wires, namely SCL and SDA.

In idle state, both lines (SCL and SDA) are high. An I$^2$C operation is started by issuing a start condition, followed by the address of the slave device. The least significant bit (LSB) of the address byte would determine whether the master will write (0) or read (1) from the slave. After all bytes are written or read, a stop condition must be issued.

Since there is no dedicated hardware for I$^2$C interface on USB245M and UM232H, there is a need to emulate it using software, i.e. bit banging. The software will directly set (or read) the state of the pins, and would handle the timing and synchronization of the signals. Although both devices (USB245M and UM232H) support bit banging, the latter was preferred because it could be configured for Multi-Protocol Synchronous Serial Engine (MPSSE) mode, which provides faster serial communication. Moreover, the USB modules have accessible drivers and libraries (found in \href{http://www.ftdichip.com/Drivers/D2XX.htm}{FTDI website}) written in C/C++, making it convenient to write program for the I$^2$C protocol. 

\subsubsection{MPSSE Mode}
The MPSSE enables interfacing various synchronous serial devices, e.g. SPI and I$^2$C, to a USB port. It allows clocking in and out of data on the specified baud rate while satisfying required timing conditions, without having the need to clock the data bit-by-bit from the host computer. A series of commands, documented in \href{http://www.ftdichip.com/Support/Documents/AppNotes/AN_108_Command_Processor_for_MPSSE_and_MCU_Host_Bus_Emulation_Modes.pdf}{Command Processor for MPSSE and MCU Host Bus Emulation Modes}, must be sent to properly control MPSSE mode. For I$^2$C interfacing, the TCK/SK (bit 0), TDI/DO (bit 1) and TDO/DI (bit 2) of ADBUS must be used for SCL and SDA lines; other data pins may be used as general-purpose input/output (GPIO). 

\subsubsection{Asynchronous Bit Bang Mode}
One disadvantage of the MPSSE mode is the restriction of bits 0-2 for SCL and SDA lines. Thus, the asynchronous bit bang mode would be necessary to utilize other pins for I$^2$C interface. In this case, a pseudo-asynchronous bit bang mode was implemented. It is so-called because the device is still configured in MPSSE mode but the host computer clocks in and out the data bit-by-bit.

\subsection{QT Application}
With the multiple devices and modules to be interfaced for the board, in terms of software, a great number of functions and subroutines must be implemented to properly communicate with each one. As a way of managing the possibly complex program, the codes were divided into modules such that each performs a single well-defined functionality. This software design technique is usually referred to as ``modular programming."

The program was developed in QT Creator, an integrated development environment (IDE) where one could create an application with a graphical user interface (GUI). There are seven (7) modules, and one (1) main program. A brief description of each module is shown below. Moreover, the main program initializes and runs the QT application.
\begin{enumerate}
    \item \textit{usb} - sets the UM232H device by opening and configuring it in MPSSE mode; uses special functions of the D2XX interface (one of the two alternative software interfaces for FTDI devices) defined in the \textit{ftd2xx} header file
	\item \textit{i2c} - has the necessary I$^2$C subroutines for both MPSSE and pseudo-asynchronous bit bang modes such as: generating start and stop conditions; transmitting and receiving data byte from the slave; and detecting the addresses of the connected slaves
    \item \textit{gui} - contains instantiation of classes and objects for the GUI, and functions that could be called in response to a particular signal (or user interaction)
    \item \textit{gpib} - provides an interface for initializing and sending commands detailed at \href{http://ecee.colorado.edu/~mathys/ecen1400/pdf/references/HP34401A_BenchtopMultimeter.pdf}{HP34401A Multimeter User's Guide} (e.g. measuring voltage, configuring the multimeter for 4-wire ohms measurement, performing a complete self-test of the multimeter) to HP34401A device; utilizes the NI-488 routines found from the \textit{ni488} header file
    \item \textit{mux\_ctrl} - implements the algorithm described in Multiplexer control section to control the analog switches connected to the daughtercard pins
    \item \textit{uni\_checker} - parses a text file containing pin configurations, and routes the inputs on the desired pins, and measures voltage or resistance, if necessary
    \item \textit{delay} - implements delay either in milliseconds or microseconds 
\end{enumerate}

A prototype of the test application is shown in Figure \ref{fig:qt-app}. Currently, it has four (4) blocks; each performs different operation as discussed below: Manual I$^2$C Interface, Multiplexer Control, HP33401A DMM via GPIB, and Automated Tester. 

\begin{enumerate}
	\item Manual I$^2$C Interface - used for communicating with MAX7311s, MAX7312s, and MAX7367s of different addresses to send and receive data over the serial data line
    \item Multiplexer Control - used for manually connecting or routing the input meters or signals to the specified pin number
    \item HP34401A DMM via GPIB - used for measuring, e.g. voltage and current, and sending different commands, such as configuring the multimeter for a certain measurement, via the General Purpose Interface Bus (GPIB)
    \item Automated Tester - used for loading a text file, consisting of different pin configurations, to display eventually the results of the test in a text file
\end{enumerate}

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[height=5.5in,keepaspectratio]{qt_app.PNG}
\par\end{centering}
\caption{QT prototype application\label{fig:qt-app}}
\end{figure}

The code repository, and the project's resources and other documents are maintained at \href{https://gitlab.com/Fredrick.Galapon/Universal_Daughtercard_Checker}{Git: Universal\_Daughtercard\_Checker}. Detailed comments were also inserted with the purpose of making the source code easier to understand. 


\section{Prototype and Testing}
To ensure the actual board's functionality, it is necessary to start with the smaller building blocks than constructing the whole main board and modules immediately. Prototyping was done first on breadboard using two evaluation kits (EV kit) of MAX7311 and the USB modules mentioned. Then, a prototype board with a USB module connector, I$^2$C channel switches, two multiplexer modules, and a daughtercard connector was constructed, and will be integrated with the developed prototype application.

\subsection{MAX7311 evaluation kit}
The \href{https://www.maximintegrated.com/en/products/interface/controllers-expanders/MAX7311EVKIT.html}{MAX7311 EV kit} provides an application and test board to exercise the features of the component. With the included software, reading and writing to MAX7311 can be done and outputs can be seen through the LEDs on the test board. Moreover, the test board allows external inputs for supply voltage and I$^2$C lines which can be useful for testing the I$^2$C interface of the USB modules.


\subsection{Prototype Board}
The prototype board is constructed in a way that wiring will be adjustable. With flexible connections, the functionality of developed codes will be tested and debugged in different ways, such as connecting the modules in different slave addresses and I$^2$C channels, and wiring the multiplexer outputs to daughtercard pins. The board layout, as shown in Figure \ref{fig:proto-brd}, has various female pin headers connected to each pin of the components. It contains pin headers and connectors for the UM232H, two MAX7367s, two multiplexer modules, and a daughtercard connector. The daughtercard connector included is dedicated for \texttt{STD\_HS\_BL} and its schematic is provided with this document. With the board, the main purpose of the project would be verified by controlling the multiplexer, routing the inputs, and acquiring readings from the meter.  

\begin{figure}[!htbp]
\begin{centering}
\includegraphics[height=3.6in,keepaspectratio]{prototype_board.PNG}
\par\end{centering}
\caption{Prototype board layout \label{fig:proto-brd}}
\end{figure}

\subsubsection{Power supply}
The board has jumper pins mainly for external power supply: 5 V, +15 V, -15 V, and GND. The $V_+$, $V_{DD}$, and $V_L$ pins of MAX7367, MAX7311, and MAX14756, respectively, are connected to the main +5 V supply of the board. While, the $V_{DD}$ and $V_{SS}$ pins of MAX14756 are wired to +15 V and -15 V, respectively. The USB module socket on the board was already set up according to the self-powered, external 5V0 supply configuration found in its \href{http://www.ftdichip.com/Support/Documents/DataSheets/Modules/DS_UM232H.pdf}{datasheet} (pg. 20); thus, the 5V0 pin is also connected to the main +5 V supply. Also, AD0 and AD1 pins of UM232H will be the I$^2$C bus lines: SCL and SDA. These were already connected to the SCL and SDA lines of MAX7367. On the other hand, the I$^2$C bus lines for the modules must be wired up to the different channels of MAX7367. And lastly, pull-up resistors for those lines were done externally.

\subsubsection{Testing of board and code functionalities}
The two modules were tested with different wire configurations for various slave addresses. Both of them work with all the listed addresses and were able to control the analog switches. With this, routing of 4-wire meter was tested along with resistors connected to the multiplexer output pins to determine whether the design and program were functional. The measurements were acquired and displayed as expected using the developed application.

\subsubsection{STD\_HS\_BL Daughtercard automated testing}
With the included connector for  daughtercard on the prototype board, initial testing was done for the daughtercard. To give a background, this daughtercard contains SPDT analog switches, buffers, and MAX7312 (similar to MAX7311). In order to check if the switches or buffers are working, communicating with multiple MAX7312s, which control the analog switches on the card, is necessary. Then, the checker will route the meters to these switches and acquire measurements.

Given the current prototype board and program, it can determine whether a pin is faulty or working using a configuration file suited for a specific daughtercard. With the automated tester, the file can be loaded to automatically acquire measurements. However, with the two multiplexer modules available, only sets of 32 pins can be tested. To test all pins, different configuration files must be loaded, and different address and channel wirings must be considered. 

The written configuration file for testing the \texttt{STD\_HS\_BL} daughtercard contains commands that controls a specific MAX7312 which connects the respective TODPs and TOOUTs (terminals of SPDT switches) of the daughtercard. If the resistance is within the range of allowable measurement (on-resistance of analog switches must be taken into account), then the switch is said to be working. 

As of now, the prototype board setup for initial testing is as follows: one module (configured as \texttt{X3} with address 0x40) is connected to channel 2, and the other module (configured as \texttt{X3} with address 0xA0) is on channel 3; both channels are from \texttt{I2CSW1}. From this, it could be noticed that only the 4-wire meter inputs may be used, and only pins 113-128 and 225-240 may be tested; nevertheless, it could be re-wired to test other desired pins. Also, the daughtercard is supplied by the external +5V and -5V power supplies and the voltage rails of the multiplexer modules are set to +/-5V due to limited external supply. 

\section{Future Work}
From the discussed design, software development, and prototype, the main board layout of the universal daughtercard checker could be finalized, and finally be submitted for PCB fabrication and assembly. 

Following this, it would be a  fully-functional universal checker if it will be able to execute an automated test for other daughtercards, just like the initial testing for the \texttt{STD\_HS\_BL} daughtercard. In this way, the board will automatically probe, acquire readings, and display results for each component. However, this will require different configuration files suited for each daughtercard which will be the test program's basis of controlling the multiplexer, running specific measurements, and feeding input signals to the card. In addition to this, serial/parallel communications, such as SPI, UART, and JTAG, are also needed by other daughtercards, and so further software development to support these protocols is necessary.

Moreover, the different adapter boards must be constructed for each daughtercard. This requires reviewing various daughtercard schematics provided. The board must include routing of multiplexer outputs, control bus, power supply, and other components that will meet the standards of each daughtercard. The test routines and functionality of the cards can be consulted with test and hardware engineers.

\end{document}
