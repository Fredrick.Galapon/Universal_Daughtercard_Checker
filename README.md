# Universal Daughtercard Checker

This universal checker project is designed to test different HSXX/BBSXX daughtercards. Basically, it is composed of a large multiplexer that can route meters and input signals to the pins of a daughtercard under test, and control modules of the daughtercard. In this way, faulty components on the card can be diagnosed.

## Getting Started
Hi. If you are reading this, you will probably continue this project. Congratulations!
We just want to tell you some tips on where to start and notes on the constructed prototype. 

* Read first the documentation. Then, you must understand all the datasheets of the main components - MAX7311, MAX14756, MAX7367, and UM232H.
* Before studying the codes, run the provided QT application to give you an overview for the control of the board. The executable file can be found at [**`codes/bin/`**](codes/bin/).
* Study the code. READ all the comments.
* Be careful with the prototype board setup. Make sure there will be no shorted connections.
* You must be provided with two UM232H (one is defective). The IC there must be replaced by the hardware repair guys.
* If you don't have the equipment, e.g. power supply, DMM, and oscilloscope, you can get them from the hardware lab. The scope, meter, and power supply probes/clips must be provided as well.
* Here are some of the contact persons:
    * Sir Richard Almendra - Engineer I, Layout
    * Sir Jefferson Padrique - Engineer II, Hardware Lab
    * Sir Frank Minas - Technician, Board Build and Repair
    * Sir JayAr Sanchez - Technician, Hardware Lab Inventory

* Enjoy!


### Prerequisites

* [FTDI CDM Drivers](resources/others/CDMv2.12.26_WHQL_Certified/) - a driver package of FTDI devices for Windows that allows direct access to a USB device via a DLL interface
* [NI-488.2 software](http://www.ni.com/download/ni-488.2-17.0/6627/en/) - a device driver software used for interfacing GPIB hardware
* [QT Creator](https://www.qt.io/ide/) - an integrated development environment (IDE) that was used in this particular project to create an application with graphical user interface (GUI)

### What's Inside

* [**`codes/`**](codes/) - contains the source codes, including the pertinent header files
* [**`doc/`**](doc/) - contains the documentation of the project
* [**`resources/`**](resources/) - contains all necessary datasheets and manuals, and schematics of some daughtercards
* [**`schematic/`**](schematic/) - contains the schematic of the main board and its modules

## Running the Application

* Open the [*untitled*](codes/untitled.pro) project in QT Creator.
* If the *Configure Project* window is shown, select the kit *Desktop QT MinGW 32 bit*.
* Click the *Projects* tab on the left-hand side of the window, and ensure that *Shadow build* is disabled.
* Select *Build* in the menu bar, and click *Run qmake*.
* Select *Build* again, and click *Rebuild all*.
* Lastly, select *Build* and click *Run*, or simply press `Ctrl+R`.

## Developers
* Gio Ishmael Evidente - Intern, Test Systems
* Fredrick Angelo Galapon - Intern, Test Systems

## Supervisor
* Bruce Allen - Senior Manager, Test Systems