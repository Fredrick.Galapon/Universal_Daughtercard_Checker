#-------------------------------------------------
#
# Project created by QtCreator 2017-07-06T16:25:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/delay.cpp \
    src/gpib.cpp \
    src/gui.cpp \
    src/i2c.cpp \
    src/main.cpp \
    src/mux_ctrl.cpp \
    src/uni_checker.cpp \
    src/usb.cpp 
#    test/test.cpp \

#CONFIG += console c++11
#CONFIG += app_bundle
#CONFIG += qt

HEADERS += \
    include/delay.h \
    include/ftd2xx.h \
    include/gpib.h \
    include/gui.h \
    include/i2c.h \
    include/mux_ctrl.h \
    include/ni488.h \
    include/uni_checker.h \
    include/usb.h

FORMS += \
    src/gui.ui

unix|win32: LIBS += -L$$PWD/lib/ -lftd2xx
unix|win32: LIBS += -L$$PWD/lib/ -lgpib

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

BASEPATH = ../codes
BUILDDIR = $${BASEPATH}/bin

CONFIG(debug, debug|release) {
    BASEPATH_OBJ = $${BASEPATH}/debug
} else {
    BASEPATH_OBJ = $${BASEPATH}/release
}
OBJECTS_DIR = $${BASEPATH_OBJ}/obj
MOC_DIR = $${BASEPATH_OBJ}/moc
RCC_DIR = $${BASEPATH_OBJ}/rcc
UI_DIR = $${BASEPATH_OBJ}/ui
DLLDESTDIR += $${BASEPATH}/bin
