#ifndef I2C_H
#define I2C_H

#include <windows.h>


/**
    This is the return value of i2c_rx() function.

    status: indicates whether the data reading was successful
    data_rx: 1 byte of data read
*/
struct I2C_RX_RETURN
{
    BOOL status;
    UCHAR data_rx;
};

/**
    Initiates a start (or repeated start) condition by
    transitioning SDA from high to low while SCL is high
    
    @ param:
        scl_bit: bit position of SCL
        sda_bit: bit position of SDA
    @ return:
        none
*/
void i2c_start(UCHAR scl_bit, UCHAR sda_bit);


/**
    Initiates a stop condition by
    transition SDA from low to high while SCL is high

    @ param:
        none
    @ return:
        none
*/
void i2c_stop(void);


/**
    Operates in MPSSE mode
    Uses bits 0-2 (TCK/SK, TDI/DO, TDO/DI) for SCL and SDA
*/
namespace mpsse {
    /**
        Transmits one byte of data

        @ param:
            data: data to be sent
        @ return:
            TRUE: successful transmission (was able to get an ACK)
            FALSE: failed transmission (failed to get an ACK)
    */
    BOOL i2c_tx(UCHAR data_tx);


    /**
        Receives one byte of data

        @ param:
            none
        @ return:
            structure containing the:
                1. status: indicates whether the data reading was successful
                2. data_rx: 1 byte of data read
    */
    I2C_RX_RETURN i2c_rx(void);
}


/**
    Operates in pseudo-asynchronous bitbang mode 
    Still in MPSSE setting, but SCL and SDA need not be connected to bits 0-2
    Uses any bit of the lower data bus for SCL and SDA
*/
namespace async {
    /**
        Transmits one byte of data

        @ param:
            data: data to be sent
        @ return:
            TRUE: successful transmission (was able to get an ACK)
            FALSE: failed transmission (failed to get an ACK)
    */
    BOOL i2c_tx(UCHAR data_tx);


    /**
        Receives one byte of data

        @ param:
            none
        @ return:
            structure containing the:
                1. status: indicates whether the data reading was successful
                2. data_rx: 1 byte of data read
    */
    I2C_RX_RETURN i2c_rx(void);
}


/**
    Identifies the connected I2C addresses

    @ param:
        scl_bit: bit position of SCL
        sda_bit: bit position of SDA
    @ return:
        list of addresses
*/
PUCHAR i2c_detect_addr(UCHAR scl_bit, UCHAR sda_bit);


#endif // I2C_H
