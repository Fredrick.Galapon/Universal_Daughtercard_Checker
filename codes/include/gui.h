#ifndef GUI_H
#define GUI_H

#include <windows.h>

#include <QMainWindow>
#include <QComboBox>        // for using a dropdown
#include <QMessageBox>      // for alerting the user, or asking the user a question
#include <QTimer>           // for calling a specific slot after a specific time interval
#include <QString>          // for using a character string
#include <QFileDialog>      // for opening a text file
#include <QFileInfo>        // for getting the file name of a text file
#include <QTextStream>      // for reading (or writing) text
#include <QProgressBar>     // for using a progress bar

extern QList<int> pin_config;   // data of pin configuration

namespace Ui {
class GUI;
}

class GUI : public QMainWindow
{
    Q_OBJECT
    QProgressBar *progressBar = new QProgressBar;
    
public:
    explicit GUI(QWidget *parent = 0);
    ~GUI();

protected:
    void showEvent(QShowEvent *event);

private slots:
    /**
        Closes the main window

        @ param:
            none
        @ return:
            none
    */
    void close(void);


    /**
        Performs initialization such as:
            1. device initialization, and
            2. I2C address detection
    
        @ param:
            none
        @ return:
            none
    */
    void reconnect_USB_module(void);


    /**
        Initiates reconnect_USB_module() function when this button is clicked

        @ param:
            none
        @ return:
            none
    */
    void on_reconnectUSBmoduleButton_clicked();


    /**
        Performs initialization for DMM

        @ param:
            none
        @ return:
            none
    */
    void reconnect_DMM(void);


    /**
        Initiates reconnect_DMM() function when this button is clicked

        @ param:
            none
        @ return:
            none
    */
    void on_reconnect_DMM_clicked(void);


    /**
        Starts the I2C operation by either writing or reading on the desired address

        @ param:
            none
        @ return:
            none
    */
    void on_i2csendButton_clicked();


    /**
        Initializes the 7311's

        @ param:
            none
        @ return:
            none
    */
    void on_muxinitButton_clicked();


    /**
        Connects the selected pin and the probe 

        @ param:
            none
        @ return:
            none
    */
    void on_muxconnectButton_clicked();


    /**
        Sends the command to GPIB

        @ param:
            none
        @ return:
            none
    */
    void on_dmmsendButton_clicked();


    /**
        Measures either voltage, current, or resistance

        @ param:
            none
        @ return:
            none
    */
    void on_dmmmeasureButton_clicked();


    /**
        Opens a dialog box to allow user to open a configuration text file
        Calls the function 'config_parse' to make an array containing pin configurations

        @ param:
            none
        @ return:
            none
    */
    void on_testeropenButton_clicked();


    /**
        Displays the results of the measurement or signal routing specified in 'pin_config' variable

        @ param:
            none
        @ return:
            none
    */
    void on_testerstartButton_clicked();

private:
    Ui::GUI *ui;
};


/**
    Converts QString to unsigned char

    QString is the data type of the content of QLineEdit.
    To be able to get the data from the text box, necessary conversion
    should be made.

    @ param:
        qstr: to be converted
    @ return:
        converted
*/
UCHAR qstring_to_uchar(QString qstr);

#endif // GUI_H
