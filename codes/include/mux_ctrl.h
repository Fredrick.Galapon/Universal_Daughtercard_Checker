#ifndef MUX_CTRL_H
#define MUX_CTRL_H

#include <windows.h>

const UCHAR WRITE =             0xFE;
const UCHAR READ =              0x01;

// MAX7367 addresses
const UCHAR CH_MUX[2] =         {0xE4, 0xE6};

// MAX7367 control bytes
const UCHAR CH_EN[4] =          {0x01, 0x02, 0x04, 0x08};
const UCHAR CH_NONE =           0x00;
const UCHAR CH_ALL =            0x0F;

// MAX7311 base addresses
const UCHAR CONN_X[5] =         {0x20, 0x30, 0x40, 0x50, 0xA0};

// MAX7311 command byte
const UCHAR PORT1_OUT =         0x02;
const UCHAR PORT2_OUT =         0x03;
const UCHAR PORT1_CONFIG =      0x06;
const UCHAR PORT2_CONFIG =      0x07;

// BBCTRL
const UCHAR BBCTRL1 =           0xD0;
const UCHAR BBCTRL2 =           0xD2;


/**
    Disables all channels of both MAX7367s

    @ param:
        none
    @ return:
        TRUE: successful
        FALSE: unsuccessful
*/
BOOL channel_dis(void);

/**
    Enables channel of MAX7367 that would be used in communicating with the MAX7311


    @ param:
        mux_sel: address of MAX7367
        ch_sel: channel of selected MAX7367
    @ return:
        TRUE: successful
        FALSE: unsuccessful
*/
BOOL channel_en(UCHAR mux_sel, UCHAR ch_sel);


/**
    Resets the MAX7311 address by:
        1. configuring all I/O as output
        2. initializing output pins to 0

    @ param:
        address: address of MAX7311 to be reset
    @ return:
        TRUE: successful
        FALSE: unsuccessful
*/
BOOL max7311_reset(UCHAR address);


/**
    Sets the output pins of MAX7311 address

    @ param:
        address: address of MAX7311 to be written
        data: two bytes of data to be written (port 1 and 2)
    @ return:
        TRUE: successful
        FALSE: unsuccessful
*/
BOOL max7311_set(UCHAR address, DWORD data);


/**
    Clears all MAX7311 outputs at power-on

    @ param:
        none
    @ return:
        TRUE: successful
        FALSE: unsuccessful
*/
BOOL switch_init(void);


/**
    Sets the swtich control outputs based on the pin number

    @ param:
        in_sel: input meter or signal (1 - 4)
        mux_sel: mux (1 - 2)
        pin: pin number (1 - 320)
    @ return:
        TRUE: sucessful switch enabling
        FALSE: unsuccesful switch enabling
*/
BOOL switch_en(DWORD in_sel, DWORD mux_sel, DWORD pin);    

#endif // MUX_CTRL_H