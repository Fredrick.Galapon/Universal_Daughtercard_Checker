#ifndef DELAY_H
#define DELAY_H

#include <windows.h>

/**
    Implements delay in milliseconds

    @ param:     
        none
    @ return:    
        none
*/
void delay_ms(DWORD msec);


/**
    Implements delay in microseconds
    
    @ param:
        none
    @ return:
        none
*/
void delay_us(DWORD usec);

#endif // DELAY_H