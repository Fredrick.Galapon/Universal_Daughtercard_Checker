#ifndef USB_H
#define USB_H

#include <windows.h>
#include "ftd2xx.h"

#define BUF_SIZE    1024

extern FT_HANDLE ftHandle;      // handle of the FTDI device
extern FT_STATUS ftStatus;      // result of each D2XX call

extern UCHAR out_buf[BUF_SIZE]; // buffer to hold MPSSE commands and data to be written
extern UCHAR in_buf[BUF_SIZE];  // buffer to hold data to be read
extern DWORD num_tosend;        // index of output buffer
extern DWORD num_toread;        // index of input buffer
extern DWORD num_sent;          // receives the number of bytes written to the device
extern DWORD num_read;          // receives the number of bytes read from the device

const DWORD CLK_FREQ = 400000;

/**
    Initlializes the device to be used by: 
        1. opening the deivce
        2. configuring the device
        3. verifying MPSSE use
        4. initializing MPSSE setting

    @ param:
        none
    @ return:
        TRUE: successful initialization
        FALSE: unsuccessful initialization
*/
BOOL usb_init(void);

#endif // USB_H
