#ifndef GPBI_H
#define GPBI_H

#include <windows.h>
#include "ni488.h"

#define CMD_SIZE    1024

extern int dev;                     // device handler
extern char command[CMD_SIZE + 1];  // used for writing to or reading from the device

const int BOARD_INDEX = 0;      // board index
const int PAD =         22;     // primary address of the device (HP34401A)
const int SAD =         0;      // secondary address of the device
const int TIMEOUT =     T10s;   // timeout value (10 seconds)
const int EOI_MODE =    1;      // asserts EOI (end or identify) with last data byte
const int EOS_MODE =    0;      // disable end-of string mode


/**
    Initializes HP34401A device with address 22

    @ param:
        none
    @ return:
        TRUE: successful initialization
        FALSE: unsuccessful initialization
*/
BOOL gpib_init(void);


/**
    Writes data bytes (stored in global variable 'command') to the device
    

    @ param:
        none
    @ return:
        TRUE: successful writing
        FALSE: unsuccessful writing
*/
BOOL gpib_wr(void);


/**
    Reads data bytes from the device and store in global variable 'command'

    @ param:
        none
    @ return
        TRUE: successful reading
        FALSE: unsuccessful reading
*/
BOOL gpib_rd(void);

#endif // GPBI_H
