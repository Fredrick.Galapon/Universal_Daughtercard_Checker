#ifndef UNI_CHECKER_H
#define UNI_CHECKER_H

#include <windows.h>

#include <QString>

const DWORD CMD_VOLT =      1000;   // voltage measurement
const DWORD CMD_RES =       1001;   // resistance measurement
const DWORD CMD_SIG =       1002;   // signal routing
const DWORD CMD_CTRL =      1003;   // control the daughtercard


/**
    Opens a text file containing either of the following:
        1. pairs of pin numbers and type of measurement
        2. input signal number and pin to be connected on the said input
        3. address of a slave (MAX7311/MAX7312) and data output for ports 1 and 2

    The configuration file must follow either of the following format:
        1. {VOLT|RES} <pin number 1>,<pin number 2>
            - measures either voltage (VOLT) or resistance (RES) across terminals <pin number 1> and <pin number 2>
        2. SIG <signal number> <pin number>
            - routes signal to specified pin number
        3. CTRL <i2c line> <address> <port 1 data> <port 2 data>
            - writes the data to the slave address conencted on the specified I2C line

    You may also put a comment on the text file by placing a '#' symbol before the comment.
    For example:
        # This is a comment
        VOLT 43, 45 # everything after the '#' symbol would be treated as a comment
        SIG 2, 152 # second argument must be from 1 to 4 only
        CTRL 2 0x40, FF, FF # this is accepted
        CTRL 3 0x42, 0xFF, 0xFF
        # this is also accepted

    Stores the pin configuration in a global variable (DWORD array) named 'pin_config' in the format:
        in1 in2, in3, in4*, in5*
            where   in1 = CMD_VOLT,         if voltage measurement
                    in1 = CMD_RES,          if resistance measurement
                    in1 = CMD_SIG,          if signal routing
                    in1 = CMD_CTRL,         if daughtercard controlling

                    in2 = pin number 1,     if measurement (must be from 1 to 320 only)
                    in2 = signal number,    if signal routing (must be from 1 to 4 only)
                    in2 = i2c line,         if daughtercard controlling (must be from 1 to 3 only)
                                                1: SCL - ADBUS2; SDA - ADBUS3
                                                2: SCL - ADBUS4; SDA - ADBUS5
                                                3: SCL - ADBUS6; SDA - ADBUS7

                    in3 = pin number 2,     if measurement (must be from 1 to 320 only)
                    in3 = pin number,       if signal routing (must be from 1 to 320 only)
                    in3 = address,          if daughtercard controlling (must be from 0x00 to 0xFF only)
                    
                    in4 = port 1 data,      if daughtercard controlling (must be from 0x00 to 0xFF only)
                    in5 = port 2 data,      if daughtercard controlling (must be from 0x00 to 0xFF only)
    * optional

    Before measurement, please connect the following:
        1. force HI - probe 1
        2. sense HI - probe 2
        3. force LO - probe 3
        4. sense LO - probe 4

    @ param:
        file_path: path of the configuration file
    @ return:
        line number of error in configuration file, if unsuccessful
        0, if successful
    */
DWORD config_parse(QString file_path);


/**
    Performs the necessary routing of the inputs and daughtercard control, and determines measurement, if necessary

    @ param:
        pin_cnt: current pin count
    @ return
        result of the test, i.e. tell whether or not the test operation was successful 
*/
QString config_test(DWORD pin_cnt);

#endif // UNI_CHECKER_H