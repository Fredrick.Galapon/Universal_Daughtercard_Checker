#include "uni_checker.h"

#include "gui.h"
#include "gpib.h"
#include "mux_ctrl.h"
#include "i2c.h"


DWORD config_parse(QString file_path)
{
    DWORD line_no = 0;  // track current line number
    int cnt, arg;       // arg is used for tracking the integer value of the argument
    QStringList list;   // arguments per line
    QRegExp delimiter("[, ]");  // match a comma or a space

    QFile file(file_path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return FALSE;
    }

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();   // reads the text file line by line
        line_no++;
        line = line.mid(0, line.indexOf("#")).trimmed();    // ignore everything after '#', and remove whitespaces on both ends
        if (line.isEmpty()) {   // check whether the string is empty
            continue;
        }
        list = line.split(delimiter, QString::SkipEmptyParts);  // parse string based on delimiter

        if (list.at(0) == "VOLT") {
            pin_config.append(CMD_VOLT);
        } else if (list.at(0) == "RES") {
            pin_config.append(CMD_RES);
        } else if (list.at(0) == "SIG") {
            pin_config.append(CMD_SIG);
            if (list.at(1).toInt(0, 10) > 4) {  // input signal must be from 1 to 4 only (checking of lower boundary is done later)
                return line_no;
            }
        } else if (list.at(0) == "CTRL") {
            pin_config.append(CMD_CTRL);
            if (list.size() != 5) { // must have 5 arguments
                return line_no;
            }
            for (cnt = 1; cnt < 5; cnt++) { 
                arg = list.at(cnt).toInt(0, 16);    // base 16
                pin_config.append(arg);
                if (cnt == 1) {
                    if ((arg < 1) || (arg > 3)) {   // i2c line
                        return line_no;
                    }
                    continue;
                }
                if ((arg < 0x00) || (arg > 0xFF)) { // must only be from 0x00 to 0xFF
                    return line_no;
                }
            }
            continue;
        } else {
            return line_no;
        }

        if (list.size() != 3) { // must have 3 arguments
            return line_no;
        }
        for (cnt = 1; cnt < 3; cnt++) { 
            arg = list.at(cnt).toInt(0, 10);    // base 10
            pin_config.append(arg);
            if ((arg < 1) || (arg > 320)) { // pin number must only be from 1 to 320
                return line_no;
            }
        }
    }

    return 0;
}


QString config_test(DWORD pin_cnt)
{
    BOOL status;
    DWORD pin_type = 1000;  // determine whether pin configuration is for mesurement, signal routing, or daughtercard controlling
    QString temp_str;
    int err;    // error in measurement
    float meas; // measured value

    if (pin_config.at(pin_cnt) == CMD_VOLT) {   // voltage measurement
        status = switch_en(1, 1, pin_config.at(pin_cnt + 1));
        status &= switch_en(3, 1, pin_config.at(pin_cnt + 2));
        strncpy(command, "MEAS:VOLT?", sizeof(command));
        pin_type = 0;
    } else if (pin_config.at(pin_cnt) == CMD_RES) { // resistance measurement
        status = switch_en(1, 1, pin_config.at(pin_cnt + 1));
        status &= switch_en(2, 1, pin_config.at(pin_cnt + 1));
        status &= switch_en(3, 1, pin_config.at(pin_cnt + 2));
        status &= switch_en(4, 1, pin_config.at(pin_cnt + 2));
        strncpy(command, "MEAS:FRES?", sizeof(command));
        pin_type = 0;
    } else if (pin_config.at(pin_cnt) == CMD_SIG) {     // signal routing
        status = switch_en(pin_config.at(pin_cnt + 1), 2, pin_config.at(pin_cnt + 2));
        pin_type = 1;
    } else if (pin_config.at(pin_cnt) == CMD_CTRL) {    // daughtercard control
        i2c_start((pin_config.at(pin_cnt + 1) * 2), (pin_config.at(pin_cnt + 1) * 2) + 1);  // set as output
        status = async::i2c_tx(pin_config.at(pin_cnt + 2) & WRITE); 
        status &= async::i2c_tx(PORT1_CONFIG);
        status &= async::i2c_tx(0x00);
        status &= async::i2c_tx(0x00);
        i2c_stop();

        i2c_start((pin_config.at(pin_cnt + 1) * 2), (pin_config.at(pin_cnt + 1) * 2) + 1);  // set output value
        status &= async::i2c_tx(pin_config.at(pin_cnt + 2) & WRITE);
        status &= async::i2c_tx(PORT1_OUT);
        status &= async::i2c_tx(pin_config.at(pin_cnt + 3));
        status &= async::i2c_tx(pin_config.at(pin_cnt + 4));
        i2c_stop();

        pin_type = 2;
    } else {
        return NULL;
    }

    if (pin_type == 0) {    // measurement
        temp_str.sprintf("pin %03d - pin %03d:\t", pin_config.at(pin_cnt + 1), pin_config.at(pin_cnt + 2));
        if (status == FALSE) {
            temp_str += "ERROR: Meters not routed";
        } else {
            status = gpib_wr(); 
            status &= gpib_rd();
            if (status == FALSE) {
                temp_str += "ERROR: Measurement failed";
            } else {
                meas = atof(command);
                strncpy(command, "*ESR?", sizeof(command)); // check whether there is reading overload
                status = gpib_wr();
                status &= gpib_rd();

                err = atoi(command);
                if (err & 0x08) {
                    temp_str += "Reading overload";
                } else {
                    temp_str.append(QString::number(meas, 'f', 9));
                    if (pin_config.at(pin_cnt) == CMD_VOLT) {
                        temp_str += " V";
                    } else {
                        temp_str += " OHM";
                    }
                }
            }
        }
    } else if (pin_type == 1) { // signal routing
        temp_str.sprintf("sig %03d - pin %03d:\t", pin_config.at(pin_cnt + 1), pin_config.at(pin_cnt + 2));
        if (status == FALSE) {
            temp_str += "ERROR: Signal not routed";
        } else {
            temp_str += "Connected";
        }
    } else if (pin_type == 2) { // daughtercard controlling
        temp_str.sprintf("addr 0x%02x (0x%02x 0x%02x):\t", pin_config.at(pin_cnt + 2), pin_config.at(pin_cnt + 3), pin_config.at(pin_cnt + 4));
        if (status == FALSE) {
            temp_str += "Unsuccessful";
        } else {
            temp_str += "Successful";
        }
    }

    return temp_str + '\n';
}
