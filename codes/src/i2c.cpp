#include "i2c.h"

#include "usb.h"

UCHAR SDAc_SCLc, SDAc_SCLs, SDAs_SCLc, SDAs_SCLs;   // clear or set
UCHAR SDAi_SCLi, SDAi_SCLo, SDAo_SCLi, SDAo_SCLo;   // input or output


void i2c_start(UCHAR scl_bit, UCHAR sda_bit)
{
    DWORD cnt;

    SDAc_SCLc = (0 << sda_bit) | (0 << scl_bit);    // clear SDA; clear SCL
    SDAc_SCLs = (0 << sda_bit) | (1 << scl_bit);    // clear SDA; set SCL
    SDAs_SCLc = (1 << sda_bit) | (0 << scl_bit);    // set SDA; clear SCL
    SDAs_SCLs = (1 << sda_bit) | (1 << scl_bit);    // set SDA; set SCL
    SDAi_SCLi = (0 << sda_bit) | (0 << scl_bit);    // SDA = in; SCL = in
    SDAi_SCLo = (0 << sda_bit) | (1 << scl_bit);    // SDA = in; SCL = out
    SDAo_SCLi = (1 << sda_bit) | (0 << scl_bit);    // SDA = out; SCL = in
    SDAo_SCLo = (1 << sda_bit) | (1 << scl_bit);    // SDA = out; SCL = out

    for (cnt = 0; cnt < 4; cnt++) {         // repeat commands to ensure start setup time is satisfied
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAs_SCLs;  // set SDA; set SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    }

    for (cnt = 0; cnt < 4; cnt++) {         // repeat commands to ensure start hold time is satisfied
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAc_SCLs;  // clear SDA; set SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    }

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAc_SCLc;  // clear SDA; clear SCL
    out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
}


void i2c_stop(void)
{
    DWORD cnt;

    for (cnt = 0; cnt < 4; cnt++) {         // repeat commands to ensure stop setup time is satisfied
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAc_SCLs;  // clear SDA; set SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    }

    for (cnt = 0; cnt < 4; cnt++) {         // repeat commands to ensure stop hold time is satisfied
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAs_SCLs;  // set SDA; set SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    }

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAc_SCLc;  // clear SDA; clear SCL
    out_buf[num_tosend++] = SDAi_SCLi;  // set TCK/SK, TDI/DO pins as input (tristate)

    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;
}


BOOL mpsse::i2c_tx(UCHAR data_tx)
{
    DWORD cnt;

    out_buf[num_tosend++] = 0x11;       // clock data bytes out (MSB first) on falling edge 
    out_buf[num_tosend++] = 0x00;       // lower byte of data length
    out_buf[num_tosend++] = 0x00;       // upper byte of data length (0x0000 means 1 byte of data)
    out_buf[num_tosend++] = data_tx;    // send data

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAc_SCLc;  // clear SCL
    out_buf[num_tosend++] = SDAi_SCLo;  // set TCK/SK as output

    out_buf[num_tosend++] = 0x22;       // clock data bits in (MSB first) on rising edge
    out_buf[num_tosend++] = 0x00;       // length of 0 means 1 bit of data
    
    out_buf[num_tosend++] = 0x87;       // command to send immediate

    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    cnt = 0;
    do {
        ftStatus = FT_GetQueueStatus(ftHandle, &num_toread);    // get the number of bytes in the input buffer
        cnt++;
    } while ((num_toread < 1) && (ftStatus == FT_OK) && (cnt < 500));

    if ((ftStatus == FT_OK) && (cnt < 500)) {       // no error occured means successful reading of data
        FT_Read(ftHandle, &in_buf, num_toread, &num_read);
        if (in_buf[0] & 0x01) {     // can't get the ACK bit
            return FALSE;
        }
    } else {
        return FALSE;
    }

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAs_SCLc;  // set SDA; clear SCL
    out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1

    return TRUE;
}


I2C_RX_RETURN mpsse::i2c_rx(void)
{
    UCHAR data_rx = 0;
    DWORD cnt;

    out_buf[num_tosend++] = 0x20;       // clock data bytes in (MSB first) on rising edge
    out_buf[num_tosend++] = 0x00;       // lower byte of data length 
    out_buf[num_tosend++] = 0x00;       // upper byte of data length (0x0000 means 1 byte of data)
    
    out_buf[num_tosend++] = 0x13;       // clock data bits out (MSB first) on falling edge
    out_buf[num_tosend++] = 0x00;       // length 0f 0x00 means 1 bit of data
    out_buf[num_tosend++] = 0x80;       // send ACK (0) / NACK (1)

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAs_SCLc;  // set SDA; clear SCL
    out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1

    out_buf[num_tosend++] = 0x87;       // command to send immediate

    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    cnt = 0;
    do {
        ftStatus = FT_GetQueueStatus(ftHandle, &num_toread);    // get the number of bytes in the input buffer
        cnt++;
    } while ((num_toread < 1) && (ftStatus == FT_OK) && (cnt < 500)); 

    if ((ftStatus == FT_OK) && (cnt < 500)) {       // no error occured means successful reading of data
        FT_Read(ftHandle, &in_buf, num_toread, &num_read);
        data_rx = in_buf[0];
    } else {
        return {FALSE, 0x00};
    }

    return {TRUE, data_rx};
}


BOOL async::i2c_tx(UCHAR data_tx)
{
    UCHAR x;
    DWORD bit, cnt;
    
    for (bit = 8; bit; bit--) {         // send bit by bit
        x = (data_tx & 0x80)? 1 : 0;    // get MSB

        for (cnt = 0; cnt < 4; cnt++) {         // repeats commands to ensure data setup time is satisfied
            out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
            out_buf[num_tosend++] = (x)? SDAs_SCLc : SDAc_SCLc; // set/clear SDA; clear SCL
            out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
        }

        for (cnt = 0; cnt < 8; cnt++) {         // repeats commands to ensure data hold time is satisfied
            out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
            out_buf[num_tosend++] = (x)? SDAs_SCLs : SDAc_SCLs; // set/clear SDA; set SCL
            out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
        }

        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = (x)? SDAs_SCLc : SDAc_SCLc; // set/clear SDA; clear SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    
        data_tx <<= 1;      
    }

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAc_SCLs;  // clear SDA; set SCL
    out_buf[num_tosend++] = SDAi_SCLo;  // set TCK/SK pins as output with bit 1

    out_buf[num_tosend++] = 0x81;       // command to read lower data byte
    out_buf[num_tosend++] = 0x87;       // command to send immediate

    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    cnt = 0;
    do {
        ftStatus = FT_GetQueueStatus(ftHandle, &num_toread);    // get the number of bytes in the input buffer
        cnt++;
    } while ((num_toread < 1) && (ftStatus == FT_OK) && (cnt < 500)); 

    if ((ftStatus == FT_OK) && (cnt < 500)) {       // no error occured means successful reading of data
        FT_Read(ftHandle, &in_buf, num_toread, &num_read);
        if (in_buf[0] & SDAs_SCLc) {    // can't get the ACK bit (1 << SDA_BIT)
            return FALSE;
        }
    } else {
        return FALSE;
    }

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = SDAs_SCLc;  // set SDA; clear SCL
    out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1

    return TRUE;
}


I2C_RX_RETURN async::i2c_rx(void)
{
    UCHAR data_rx = 0;
    DWORD bit, cnt;

    for (bit = 0; bit < 8; bit++) {
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAs_SCLc;  // set SDA; clear SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    
        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAs_SCLs;  // set SDA; set SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    
        out_buf[num_tosend++] = 0x81;       // command to read lower data byte
        out_buf[num_tosend++] = 0x87;       // command to send immediate    
        
        FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
        num_tosend = 0;

        cnt = 0;
        do {
            ftStatus = FT_GetQueueStatus(ftHandle, &num_toread);    // get the number of bytes in the input buffer
            cnt++;
        } while ((num_toread < 1) && (ftStatus == FT_OK) && (cnt < 500)); 

        if ((ftStatus == FT_OK) && (cnt < 500)) {       // no error occured means successful reading of data
            FT_Read(ftHandle, &in_buf, num_toread, &num_read);
            data_rx <<= 1;
            if (in_buf[0] & SDAs_SCLc) {
                data_rx |= 1;
            }
        } else {
            return {FALSE, 0x00};
        }

        out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
        out_buf[num_tosend++] = SDAs_SCLc;  // set SDA; clear SCL
        out_buf[num_tosend++] = SDAo_SCLo;  // set TCK/SK, TDI/DO pins as output with bit 1
    }

    return {TRUE, data_rx};
}


PUCHAR i2c_detect_addr(UCHAR scl_bit, UCHAR sda_bit)
{
    DWORD cnt, index = 0;
    UCHAR addr = 0;
    PUCHAR list_addr = (PUCHAR) malloc(sizeof(UCHAR) * 70);

    for (cnt = 0; cnt < 70; cnt++) {    // 64 possible 7311 addresses; also include 7367 addresses (0xE4 & 0xE6)
        i2c_start(scl_bit, sda_bit);
        switch (cnt) {
            case 0:         // first address
                addr = 0x20;
                break;
            case (64/2):    // middle address
                addr = 0xA0;
                break;
            default:        // other addresses
                addr += 2;
                break;
        }

        if (async::i2c_tx(addr) == TRUE) {  // was able to get an ACK
            list_addr[index++] = addr;      // add to the list of addresses
        }
    }

    list_addr[index] = '\0';

    return list_addr;
}
