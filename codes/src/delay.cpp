#include "delay.h"
#include <time.h>

void delay_ms(DWORD msec)
{
    clock_t goal = msec + clock();
    while (goal > clock());
}

void delay_us(DWORD usec)
{
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10*usec);

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
}
