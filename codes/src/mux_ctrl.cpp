#include "mux_ctrl.h"

#include "i2c.h"


BOOL channel_dis(void)
{
    BOOL status;
    DWORD cnt;

    for (cnt = 0; cnt < 2; cnt++) { // for each mux
        i2c_start(0, 1);
        status = async::i2c_tx(CH_MUX[cnt] & WRITE);
        status &= async::i2c_tx(CH_NONE);
        i2c_stop();
    }

    return status;
}


BOOL channel_en(UCHAR mux_sel, UCHAR ch_sel)
{
    BOOL status;

    i2c_start(0, 1);
    status = async::i2c_tx(mux_sel & WRITE);
    status &= async::i2c_tx(ch_sel);
    i2c_stop();

    return status;
}


BOOL max7311_reset(UCHAR address)
{
    BOOL status;

    i2c_start(0, 1);
    status = async::i2c_tx(address & WRITE);
    status &= async::i2c_tx(PORT1_CONFIG);
    status &= async::i2c_tx(0x00);  
    status &= async::i2c_tx(0x00);  // configure all pins as outputs
    i2c_stop();

    i2c_start(0, 1);
    status &= async::i2c_tx(address & WRITE);
    status &= async::i2c_tx(PORT1_OUT);
    status &= async::i2c_tx(0xFF);  // set all pins (logic 1) to open switches of 14756
    status &= async::i2c_tx(0xFF);
    i2c_stop();

    return status;
}


BOOL max7311_set(UCHAR address, DWORD data)
{
    BOOL status;

    i2c_start(0, 1);
    status = async::i2c_tx(address & WRITE);
    status &= async::i2c_tx(PORT1_OUT);
    status &= async::i2c_tx(~(data & 0x00FF));  // write lower bits 
    status &= async::i2c_tx(~((data >> 8) & 0x00FF));   // write upper bits
    i2c_stop();

    return status; 
}


BOOL switch_init(void)
{
    BOOL status;
    DWORD conn, in;

    status = channel_dis();
    status &= channel_en(CH_MUX[0], CH_ALL);    // enable all channels
    status &= channel_en(CH_MUX[1], CH_ALL);    // enable all channels
    for (conn = 0; conn < 5; conn++) {  // for each conn
        for (in = 0; in < 4; in++) { // reset all 7311 per module
            status &= max7311_reset(CONN_X[conn] + (2 * in));
        }
    }
    status &= channel_dis();
    
    return status;
}


BOOL switch_en(DWORD in_sel, DWORD mux_sel, DWORD pin)
{
    BOOL status;
    DWORD mux, ch, conn, in, bit, data;
    DWORD conn_rst;

    mux = mux_sel - 1;
    ch = (pin - 1) / 80;
    conn = ((pin - 1) % 80) / 16;
    in = in_sel - 1;
    bit = ((pin - 1) % 80) % 16;
    data = 0x0001 << bit;

    status = channel_dis();
    status &= channel_en(CH_MUX[mux], CH_ALL);
    for (conn_rst = 0; conn_rst < 5; conn_rst++) {
        /**
            PLEASE READ!!! THIS IS IMPORTANT!
            
            The purpose of the next line is to ensure that no different pins would be 
            shorted to a single input. However, as of now, only two modules were used 
            in prototyping, and so the value of 'status' in
            "status &= max7311_reset(CONN_X[conn_rst] + (2 * in));"
            would always be FALSE, regardless of whether its value beforehand is
            TRUE or FALSE, because other addresses could not be detected.

            So, if the main board is ready, MAKE SURE TO INCLUDE
            "status &= " at the BEGINNING. 
        */
        max7311_reset(CONN_X[conn_rst] + (2 * in));     // reset so that no other pins are connected to the probe
        // status &= max7311_reset(CONN_X[conn_rst] + (2 * in));   // reset so that no other pins are connected to the probe
    }

    if (status == FALSE) {
        return FALSE;
    }
    
    status &= channel_en(CH_MUX[mux], CH_EN[ch]);
    status &= max7311_set((CONN_X[conn] + (2 * in)), data);
    status &= channel_dis();

    return status;
}
