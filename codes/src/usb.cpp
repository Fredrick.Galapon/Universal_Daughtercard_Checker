#include "usb.h"

#include "ftd2xx.h"

FT_HANDLE ftHandle;         
FT_STATUS ftStatus;

UCHAR out_buf[1024], in_buf[1024];
DWORD num_tosend, num_toread, num_sent, num_read;


BOOL usb_init(void)
{
    /**
        open the device
    */
    ftStatus = FT_Open(0, &ftHandle);       // open the port

    /**
        configure the device
    */
    ftStatus = FT_ResetDevice(ftHandle);    // reset USB device

    // purge USB receive buffer first by reading out all old data
    ftStatus |= FT_GetQueueStatus(ftHandle, &num_toread);
    if ((ftStatus == FT_OK) && (num_toread > 0)) {
        FT_Read(ftHandle, &in_buf, num_toread, &num_read);
    }

    ftStatus |= FT_SetUSBParameters(ftHandle, 65536, 65535);                // set USB request transfer sizes to 64K
    ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);                  // disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 0, 5000);                          // sets the read and write timeout in ms
    ftStatus |= FT_SetLatencyTimer(ftHandle, 1);                            // set the latency timer to 1ms (default is 16ms)
    ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0x00, 0x00);   // turn on flow control to synchronize in requests
    ftStatus |= FT_SetBitMode(ftHandle, 0x00, 0x00);    // reset controller
    ftStatus |= FT_SetBitMode(ftHandle, 0Xff, 0x02);    // enable MPSSE mode

    if (ftStatus != FT_OK) {
        return FALSE;
    }

    /**
        verify MPSSE mode
    */
    out_buf[num_tosend++] = 0xAB;                               // send bad command 0xAB
    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);         // send off the bad command
    num_tosend = 0;                                             // clear output buffer
    
    do {
        ftStatus = FT_GetQueueStatus(ftHandle, &num_toread);    // get the number of bytes in the input buffer
    } while ((num_toread == 0) && (ftStatus == FT_OK));

    bool cmd_echo = false;
    FT_Read(ftHandle, &in_buf, num_toread, &num_read);          // read out the data from input buffer

    DWORD cnt;
    for (cnt = 0; cnt < num_read - 1; cnt++) {                  // check if bad and echo commands were received
        if ((in_buf[cnt] == 0xFA) && (in_buf[cnt + 1] == 0xAB)) {
            cmd_echo = true;
            break;
        }
    }

    if (cmd_echo == false) {
        return FALSE;
    }

    /**
        initialize the MPSSE setting for I2C communication
    */
    out_buf[num_tosend++] = 0x9E;       // command to enable tristate 
    out_buf[num_tosend++] = 0xFF;       // lower byte (AD0-7)
    out_buf[num_tosend++] = 0xFF;       // upper byte (AC0-7)
    out_buf[num_tosend++] = 0x8A;       // command to disable clock divide by 5 for 60MHz master clock
    out_buf[num_tosend++] = 0x97;       // command to turn off adaptive clocking
    out_buf[num_tosend++] = 0x8D;       // command to enable 3-phase data clock; used by I2C to allow data on both clock edges
    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    out_buf[num_tosend++] = 0x80;       // command to set directions of lower 8 pins and force value on bits set as output
    out_buf[num_tosend++] = 0x00;       // clear all
    out_buf[num_tosend++] = 0xFF;       // set all as output with bit 1

    DWORD clock_div = (60000000 / (2 * CLK_FREQ)) - 1;  // derived from the formula given in the manual
    out_buf[num_tosend++] = 0x86;                       // command to set clock divisor
    out_buf[num_tosend++] = clock_div & 0xFF;           // set lower byte of clock divisor
    out_buf[num_tosend++] = (clock_div >> 8) & 0xFF;    // set upper byte of clock divisor
    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    out_buf[num_tosend++] = 0x85;       // command to turn off loop back of TDI/DO connection
    FT_Write(ftHandle, out_buf, num_tosend, &num_sent);
    num_tosend = 0;

    return TRUE;
}
