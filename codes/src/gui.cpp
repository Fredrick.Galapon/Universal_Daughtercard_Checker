#include "gui.h"
#include "ui_gui.h"

#include "usb.h"
#include "gpib.h"
#include "i2c.h"
#include "mux_ctrl.h"
#include "uni_checker.h"

UCHAR scl_bit, sda_bit;     // bit positions of SCL and SDA
UCHAR usb_init_flag = 0, dmm_init_flag = 0; // flags to indicate whether USB module and DMM initializations were successful or not
QString temp_str;           // temporary storage of character string.
QString file_name;          // file name of the opened configuration file
QList<int> pin_config({});  // data of pin configuration


GUI::GUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GUI)
{
    ui->setupUi(this);
    ui->statusBar->setSizeGripEnabled(false);

    // connect as follows to call the specified slot when a menu in the menu bar is clicked
    connect(ui->actionReconnect_USB_Module, SIGNAL(triggered()), this, SLOT(reconnect_USB_module()));
    connect(ui->actionReconnect_DMM, SIGNAL(triggered()), this, SLOT(reconnect_DMM()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));

    // initialize a progress bar
    progressBar->setMaximumHeight(16);
    progressBar->setMaximumWidth(300);
    progressBar->setHidden(true);
    progressBar->setValue(0);
    ui->statusBar->addPermanentWidget(progressBar, 0);

    // Manual I2C interface: only allow hexadecimal values and 2 characters
    QRegExp rx("[0-9a-fA-F]{2}");
    ui->dataTextbox->setValidator(new QRegExpValidator(rx, this));
    ui->addressDropdown->setValidator(new QRegExpValidator(rx, this));
}


GUI::~GUI()
{
    delete ui;
}


void GUI::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);                                  // call a slot after a specified interval
    QTimer::singleShot(100, this, SLOT(reconnect_USB_module()));    // USB module connection at startup
    QTimer::singleShot(100, this, SLOT(reconnect_DMM()));           // DMM connection at startup
    return;
}


void GUI::close()
{
    // slot for quit option in the menu bar
    delete ui;
    exit(0);
}


void GUI::reconnect_USB_module(void)
{
    // initialize device
    if (usb_init() == FALSE) {      // failed initialization of USB module
        usb_init_flag = 0;
        ui->statusBar->showMessage("ERROR: USB module not found");

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Connect to USB Module", "USB module was not found. Keep trying to connect?",
                                        QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            on_reconnectUSBmoduleButton_clicked();
        }

        return;
    }

    usb_init_flag = 1;

    // set I2C bus lines, scl and sda. based on ADBUS connection
    scl_bit = ui->sclValue->value();
    sda_bit = ui->sdaValue->value();

    if (scl_bit == sda_bit) {
        ui->statusBar->showMessage("ERROR: SCL and SDA bits are the same.", 2000);
        return;
    }

    ui->statusBar->showMessage("USB module found. Detecting I2C devices...");

    // detect I2C addreeses
    PUCHAR list_addr = i2c_detect_addr(scl_bit,sda_bit);   // detect connected I2C addresses
    DWORD index;

    ui->addressDropdown->clear();      // clear all items
    for (index = 0; list_addr[index] != '\0'; index++) {    // make a dropdown for the connected I2C addresses
        ui->addressDropdown->addItem(QString("%1").arg(list_addr[index], 0, 16).toUpper());
    }

    if (index) {
        ui->statusBar->showMessage("I2C devices found", 3000);
    } else {
        ui->statusBar->showMessage("ERROR: No I2C devices found");
    }
}


void GUI::on_reconnectUSBmoduleButton_clicked()
{
    ui->statusBar->showMessage("Reconnecting...");
    QTimer::singleShot(100, this, SLOT(reconnect_USB_module()));    // call a slot after a specified interval
}


void GUI::on_i2csendButton_clicked()
{
    BOOL status;

    // parse I2C address and data
    UCHAR data = qstring_to_uchar(ui->dataTextbox->text());
    UCHAR address = qstring_to_uchar(ui->addressDropdown->currentText());

    if (!usb_init_flag) {
        ui->statusBar->showMessage("ERROR: USB module not connected", 2000);
        return;
    }

    // set I2C bus lines, SCL and SDA (based on ADBUS connection)
    scl_bit = ui->sclValue->value();
    sda_bit = ui->sdaValue->value();

    if (scl_bit == sda_bit) {
        ui->statusBar->showMessage("ERROR: SCL and SDA bits are the same.", 2000);
        return;
    }

    temp_str.sprintf("SCL at bit%d, SDA at bit%d", scl_bit, sda_bit);
    ui->statusBar->showMessage(temp_str, 1000);

    i2c_start(scl_bit, sda_bit);
    status = async::i2c_tx(address & WRITE);

    if (ui->max7311Group->isChecked()) {
        ui->max7311Group->setVisible(true);

        status &= async::i2c_tx(PORT1_CONFIG);
        status &= async::i2c_tx(0x00);
        status &= async::i2c_tx(0x00);
        i2c_stop();

        i2c_start(scl_bit, sda_bit);
        status &= async::i2c_tx(address & WRITE);
        if (ui->portGroup->checkedId() == -2) { // port 1
            status &= async::i2c_tx(PORT1_OUT);
        } else {    // port 2
            status &= async::i2c_tx(PORT2_OUT);
        }
    }

    if (ui->cmdGroup->checkedId() == -2) {  // write command
        status &= async::i2c_tx(data);
        if (status == TRUE) {
            ui->statusBar->showMessage("Writing successful", 1500);
        } else {
            ui->statusBar->showMessage("ERROR: Writing unsuccessful", 1500);
        }
    } else {    // read command
        ui->dataTextbox->clear();
        i2c_start(scl_bit, sda_bit);    // repeated start
        status &= async::i2c_tx(address | READ);
        I2C_RX_RETURN read = async::i2c_rx();
        status &= read.status;
        if (status == TRUE) {
            ui->dataTextbox->setText(QString().sprintf("%02x", read.data_rx));
            ui->statusBar->showMessage("Reading successful", 1500);
        } else {
            ui->statusBar->showMessage("ERROR: Reading unsuccessful", 1500);
        }
    }
    i2c_stop();
}


void GUI::reconnect_DMM(void)
{
    if (gpib_init() == FALSE) {     // failed to connect to DMM
        dmm_init_flag = 0;
        ui->statusBar->showMessage("ERROR: DMM not found");

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Connect to DMM", "DMM was not found. Keep trying to connect?",
                                        QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            on_reconnect_DMM_clicked();
        }

        return;
    } 
    
    dmm_init_flag = 1;
    ui->statusBar->showMessage("DMM connected", 3000);
}


void GUI::on_reconnect_DMM_clicked()
{
    ui->statusBar->showMessage("Reconnecting...");
    QTimer::singleShot(100, this, SLOT(reconnect_DMM()));     // call a slot after a specified interval
}


UCHAR qstring_to_uchar(QString qstr)
{
    UCHAR hex = 0;
    DWORD cnt, x;

    PCSTR str = qstr.toUtf8().constData();  // convert to const char *

    for (cnt = 0; cnt < strlen(str); cnt++) { // two characters
        if ((str[cnt] >= '0') && (str[cnt] <= '9')) {   // ASCII code of '0' is 48
            x = str[cnt] - 48;
        } else if ((str[cnt] >= 'A') && (str[cnt] <= 'F')) {    // ASCII code of 'A' is 65; A in hex is 10 in decimal
            x = str[cnt] - 65 + 10;
        } else {    // ASCII code of 'a' is 97; a in hex is 10 in decimal
            x = str[cnt] - 97 + 10;
        }

        hex = (hex << 4) | x;
    }

    return hex;
}


void GUI::on_muxinitButton_clicked()
{
    BOOL status;

    if (!usb_init_flag) {
        ui->statusBar->showMessage("ERROR: USB module not connected", 2000);
        return;
    } 

    status = channel_dis();
    status &= switch_init();
    status &= channel_dis();

    if (status == TRUE) {
        ui->statusBar->showMessage("Multiplexer initialized", 2000);
    } else {
        ui->statusBar->showMessage("ERROR: Multiplexer not initialized", 2000);
    }
}


void GUI::on_muxconnectButton_clicked()
{
    DWORD in = 0, mux = 1, pin;
    pin = ui->muxpinValue->value();

    if (!usb_init_flag) {
        ui->statusBar->showMessage("ERROR: USB module not connected", 2000);
        return;
    }

    if (ui->meter1Button->isChecked()) {
        in = 1;
        mux = 1;
    } else if (ui->meter2Button->isChecked()) {
        in = 2;
        mux = 1;
    } else if (ui->meter3Button->isChecked()) {
        in = 3;
        mux = 1;
    } else if (ui->meter4Button->isChecked()) {
        in = 4;
        mux = 1;
    } else if (ui->signal1Button->isChecked()) {
        in = 1;
        mux = 2;
    } else if (ui->signal2Button->isChecked()) {
        in = 2;
        mux = 2;
    } else if (ui->signal3Button->isChecked()) {
        in = 3;
        mux = 2;
    } else if (ui->signal4Button->isChecked()) {
        in = 4;
        mux = 2;
    }

    if (!in) {
        ui->statusBar->showMessage("ERROR: Multiplexer input not selected", 2000);
        return;
    }

    if (switch_en(in, mux, pin) == FALSE) {
        ui->statusBar->showMessage("Unable to connect multiplexer input", 2000);
        return;
    }

    if (mux == 1) {
        temp_str.sprintf("Meter %lu selected. Connected to pin %lu.", in, pin);
    } else if (mux == 2) {
        temp_str.sprintf("Signal %lu selected. Connected to pin %lu.", in, pin);
    }
    ui->statusBar->showMessage(temp_str, 2000);
}


void GUI::on_dmmsendButton_clicked()
{
    BOOL status;
    char *query;
    int err;

    if (!dmm_init_flag) {
        ui->statusBar->showMessage("ERROR: DMM not connected", 2000);
        return;
    } 

    if (ui->dmmcommandEdit->toPlainText().isEmpty() == TRUE) {
        ui->statusBar->showMessage("ERROR: Enter DMM commands", 2000);
        return;
    } 
    
    ui->dmmoutputEdit->clear();
    strncpy(command, ui->dmmcommandEdit->toPlainText().toStdString().c_str(), sizeof(command));

    if (gpib_wr() == FALSE) {
        ui->statusBar->showMessage("ERROR: DMM write failed", 1000);
        return;
    }

    query = strchr(command, '?');
    if (query != NULL) {    // determine whether the command is a query
        if (gpib_rd() == FALSE) {
            ui->statusBar->showMessage("ERROR: DMM read failed", 1000);
            return;
        } 
        
        temp_str.sprintf("Read: %sMeas: %01.9f", command, atof(command));
        ui->dmmoutputEdit->setPlainText(temp_str);
        ui->statusBar->showMessage("DMM command sent", 1000);

        strncpy(command, "*ESR?", sizeof(command));
        status = gpib_wr();
        status &= gpib_rd();
        if (status == FALSE) {
            ui->statusBar->showMessage("Error detection failed", 2000);
            return;
        }
        
        err = atoi(command);
        if (err & 0x08) {   // error detection
            ui->statusBar->showMessage("DMM ERROR: Device error (self-test or reading overload error)", 3000);
        }
    }
}

void GUI::on_dmmmeasureButton_clicked()
{
    BOOL status;
    char meas[20], unit[4];

    if (!dmm_init_flag) {
        ui->statusBar->showMessage("ERROR: DMM not connected", 2000);
        return;
    }
    
    ui->dmmoutputEdit->clear();

    if (ui->dmmmeasureDropdown->currentText() == "Voltage") {   // dc voltage measurement
        strncpy(meas, "DC voltage", sizeof(meas));
        strncpy(command, "MEAS:VOLT:DC?", sizeof(command));
        strncpy(unit, "V", sizeof(unit));
    } else if (ui->dmmmeasureDropdown->currentText() == "Current") {    // dc current measurement
        strncpy(meas, "DC current", sizeof(meas));
        strncpy(command, "MEAS:CURR:DC?", sizeof(command));
        strncpy(unit, "A", sizeof(unit));
    } else if (ui->dmmmeasureDropdown->currentText() == "Resistance (2W)") {    // resistance (2W) measurement
        strncpy(meas, "Resistance (2W)", sizeof(meas));
        strncpy(command, "MEAS:RES?", sizeof(command));
        strncpy(unit, "OHM", sizeof(unit));
    } else if (ui->dmmmeasureDropdown->currentText() == "Resistance (4W)") {    // resistance 4W measurement
        strncpy(meas, "Resistance (4W)", sizeof(meas));
        strncpy(command, "MEAS:FRES?", sizeof(command));
        strncpy(unit, "OHM", sizeof(unit));
    }

    temp_str.sprintf("Measuring %s...", meas);
    ui->statusBar->showMessage(temp_str, 1000);

    status = gpib_wr();
    status &= gpib_rd();
    if (status == FALSE) {
        ui->statusBar->showMessage("ERROR: DMM write failed", 1000);
        return;
    }

    temp_str.sprintf("Meas: %01.9f %s", atof(command), unit);
    ui->dmmoutputEdit->setPlainText(temp_str);

    temp_str.sprintf("%s read", meas);
    ui->statusBar->showMessage(temp_str, 1000);
}


void GUI::on_testeropenButton_clicked()
{
    DWORD line_err;
    pin_config = {};

    QString file_path = QFileDialog::getOpenFileName(this, tr("Open configuration text file"), "config/", tr("Text Files (*.txt)"));

    QFileInfo file_info(file_path);
    file_name = file_info.fileName();   // gets the file name only

    if (file_path == "") {
        return;
    }

    line_err = config_parse(file_path);
    if (line_err) {
        ui->statusBar->showMessage("ERROR: Parsing text file " + file_name + " failed at line " + QString::number(line_err), 3000);
        pin_config = {};
    } else {
        ui->statusBar->showMessage("Successful parsing of text file " + file_name, 1500);
    }
}


void GUI::on_testerstartButton_clicked()
{
    int cnt;
    QString result;

     if (!usb_init_flag) {
         ui->statusBar->showMessage("ERROR: USB module not connected", 2000);
         return;
     }

     if (!dmm_init_flag) {
         ui->statusBar->showMessage("ERROR: DMM not connected", 2000);
         return;
     }

     if (!pin_config.size()) {
         ui->statusBar->showMessage("ERROR: No configuration text file successfully loaded", 2000);
         return;
     }

    QMessageBox::information(this, "Warning",
                            "Please make sure that all 4 probes are connected as follows:\n\
force HI - probe 1\nsense HI - probe 2\nforce LO - probe 3\nsense LO - probe 4");

    progressBar->setValue(0);
    progressBar->setHidden(false);
    ui->statusBar->showMessage("Operation completing...");
    progressBar->setRange(0, pin_config.size()-1);

    for (cnt = 0; cnt < pin_config.size(); cnt++) {
        result += config_test(cnt);
        progressBar->setValue(cnt);
    }

    ui->statusBar->showMessage("Operation completed", 3000);
    progressBar->setHidden(true);

    QString text_file = QFileDialog::getSaveFileName(this, tr("Save result in file"),
                                                     "result/" + file_name.mid(0, file_name.indexOf(".txt")) + "_result",
                                                     tr("Text Files (*.txt)"));

    if (text_file != NULL) {
        QFile file(text_file);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << result;
        file.close();

        QMessageBox::information(this, "Result", "Result saved in file " + text_file);
    }
}
