#include "gpib.h"

#include "ni488.h"

int dev;                     
char command[CMD_SIZE + 1];


BOOL gpib_init(void)
{
    dev = ibdev(BOARD_INDEX, PAD, SAD, TIMEOUT, EOI_MODE, EOS_MODE);    // open the device
    
    if (ibclr(dev) & ERR) {     // clear device
        return FALSE;
    }

    strncpy(command, "*ESE 255;", sizeof(command));    // enable standard event enable registers
    strncat(command, "STAT:QUES:ENAB 32767;", sizeof(command));     // enable questionable data registers
    strncat(command, "*CLS", sizeof(command));      // clear the bits of the event registers

    return gpib_wr();
}

BOOL gpib_wr(void)
{
    if (ibwrt(dev, command, strlen(command)) & ERR) {   // write data bytes to the device
        return FALSE;
    }
    
    return TRUE;
}


BOOL gpib_rd(void)
{
    memset(command, 0, sizeof(command));

    if (ibrd(dev, command, CMD_SIZE) & ERR) {   // read data bytes from the device
        return FALSE;
    }

    return TRUE;
}
