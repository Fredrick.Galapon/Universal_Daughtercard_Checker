### What's Inside

* [**`bin/`**](bin/) - contains the output executable files
* [**`config/`**](config/) - contains the configuration files, i.e., the pins to be tested
* [**`debug/`**](debug/) - contains all object files during debug mode
* [**`include/`**](include/) - contains the header files
* [**`lib/`**](lib/) - contains the libraries needed in the compilation of the application
* [**`release/`**](release/) - contains all object files during release mode 
* [**`result/`**](result/) - contains the results of the tests (default directory)
* [**`src/`**](src/) - contains the source code files of the application
* [**`test/`**](test/) - contains test code files