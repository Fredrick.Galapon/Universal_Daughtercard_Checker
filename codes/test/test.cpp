#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "gpib.h"

int main(void) 
{
    double meas;
    int err;
    char *query;

    if (gpib_init() == FALSE) {
        printf("Unable to initialize\n");
        return 1;
    }

    while (1) {
        printf("Command: ");
        fgets(command, sizeof(command), stdin);

        if (gpib_wr() == FALSE) {
            printf("Unable to write\n");
            continue;
        }

        query = strchr(command, '?');
        if (query != NULL) {    // determine whether the command is a query
            if (gpib_rd() == FALSE) {
                printf("Unable to write\n");
                continue;
            }

            meas = atof(command);
            printf("Reading: %s", command);
            printf("Measurement: %.8lf\n", meas);
        }
        
        strncpy(command, "STAT:QUES:EVEN?", sizeof(command));
        gpib_wr();
        gpib_rd();
        err = atoi(command);

        if (err > 0) {
            switch (err) {
                case 1:
                    printf("Voltage overload\n");
                    break;
                case 2:
                    printf("Current overload\n");
                    break;
                case 512:
                    printf("Ohms overload\n");
                    break;
                default:
                    printf("Other error detected\n");
                    break;
            }
        }

        printf("\n");
    }

    ibonl(dev, 0);

    return 0;
}
